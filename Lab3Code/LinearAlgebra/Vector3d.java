//Domenico Cusucna 2038364

package LinearAlgebra;
import java.lang.Math;

public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Get Methods
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    //Magnitude
    public double magnitude(){
        
        double result;

        result = Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z,2);
        result = Math.sqrt(result);

        return result;
    }

    //Dot Product
    public double dotProduct(Vector3d dots){    
        double dotsX = this.x * dots.getX();
        double dotsY = this.y * dots.getY();
        double dotsZ = this.z * dots.getZ();
        double dotProduct = dotsX + dotsY + dotsZ;

        return dotProduct;
    }

    //Add
    public Vector3d add(Vector3d adding){
        double addX = this.x + adding.getX();
        double addY = this.y + adding.getY();
        double addZ = this.z + adding.getZ();

        Vector3d addedVector = new Vector3d(addX, addY, addZ);

        return addedVector;
    }

}