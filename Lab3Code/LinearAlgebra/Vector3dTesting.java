//Domenico Cuscuna 2038364

package LinearAlgebra;

public class Vector3dTesting {
    public static void main(String[] args) {
        Vector3d testing = new Vector3d(4, 2, 3);
        Vector3d testing2 = new Vector3d(10,4,6);
        
        //Used to test the add method
        Vector3d checkResults = new Vector3d(0,0,0);




        //Testing out the magnitude method
        System.out.println(testing.magnitude());

        //Testing out the dotProduct method
        System.out.println(testing.dotProduct(testing2));

        //Testing out the add method
        checkResults = testing.add(testing2);

        System.out.println(checkResults.getX());
        System.out.println(checkResults.getY());
        System.out.println(checkResults.getZ());
    }    
}
