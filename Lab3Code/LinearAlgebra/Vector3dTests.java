//Domenico Cuscuna 2038364

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class Vector3dTests{
    
    @Test

    public void testGet(){
        Vector3d testingGet = new Vector3d(3,5,7);
        assertEquals(3, testingGet.getX());
        assertEquals(5, testingGet.getY());
        assertEquals(7, testingGet.getZ());
    }


    @Test

    public void testMagnitude(){
        Vector3d testingMag = new Vector3d(5,6,2);
        assertEquals(8.06225774829855,testingMag.magnitude());
    }

    @Test

    public void testDotProduct(){
        Vector3d testingDot = new Vector3d(2, 4, 6);
        Vector3d testingDot2 = new Vector3d(5, 7, 2);

        assertEquals(50,testingDot.dotProduct(testingDot2));
    }

    @Test

    public void testAdd(){
        Vector3d testingAdd = new Vector3d(2, 4, 6);
        Vector3d testingAdd2 = new Vector3d(5, 7, 2);

        Vector3d result = testingAdd.add(testingAdd2);

        assertEquals(7, result.getX()); 
        assertEquals(11, result.getY());
        assertEquals(8, result.getZ());
    }
}
